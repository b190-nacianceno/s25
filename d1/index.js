console.log("Hello World")
// SECTION - JSON Objects
/* 
-JSON stands for JavaScript Object Notation
-JSON is also used in other programming languages, hence "notation" in its name
-JSON is not to be confused with JS objects since aside from the format itself, JS objects are exvlusive to JS while JSON can be used by other languages as well

SYNTAX:
{
    "propertyA":"valueA"
    "propertyB":"valueB"

}
*/

// below is an example of JSON
/* 
-when logged in the console, it seems that the apperance of JSON does not differ from JS Objects. But JSON can be used in other languages
*/

/* 
WHAT JSON DOES
    -for serializing different data types into bytes (unit of data 1s and 0s)
    -bytes is a unit of data that is composed of eight binary digits(1/0) that is used to represent a character (letters, numbers, typographical symbol)
    -serialization is the process of converting data into a series of bytes for easier transmission/transer of information

*/


let cities = [{
    "city": "Quezon City",
    "province": "Metro Manila",
    "country": "Philippines"

},
{
    "city": "Manila City",
    "province": "Cebu",
    "country": "Philippines" 
},
{city: "Cagayan de Oro City",
    "province": "Misamis Oriental",
    "country": "Philippines"
}
]

console.log(cities);

// JSON Methods
    // JSON object contains methods for parsing and converting data into/from JSON or stringified JSON

// SECTION - Stringify Method
/* 
-used to convert JS objects into a string
-a stringified JSON is a JS object converted into a string but in JSON format to be used in other langs or functions of a JS application
-commonly used in HTTP requests where information is required to be sent and received in a stringified JSON format
-requests are an important part of programming where an application communicates with a backend application to perform different tasks such getting/creating data in a database
-a fronend application is an apppllication that is used to interact with users to perform different tasks and display information while the backend app are commonly used for all business logic and data processing
- a database normally stores information/data that can be used in most application
- a commonly stored data in databases are user information, transaction records, and product information
-Node/Express JS are two of technologies that are used for creating backend applications which process requests from frontend application
    -Node JS is a Java Runtime Environment (JRE)/ software that is made to execute other software
    -Express JS is a Node JS framework that provides features for easily creating web and mobile applications
*/
let batchesArr = [ {batchName: "Batch X"}, {batchName: "Batch Y"}];
console.log(batchesArr);
// Stringify Method
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));


// let data = {
//     name: "Elon Musk",
//     age: 50,
//     address: {
//         city: "New York City",
//         country: "USA"
//     }
// }


// console.log(JSON.stringify(data));

let data = JSON.stringify({
    name: "Elon Musk",
    age: 50,
    address: {
        city: "New York City",
        country: "USA"}
});

console.log(data);

// an example where JSON is commonly used is on package.json files which an express JS application uses to keep track of the infomration rearding a repository/project (see package.json)


// JSON.stringify with prompt()

/* 
-when information is stored in a variable and is not hard coded into an object that is being stringifies, we can supply the value with a variable
-the property name and the value name would have to be the same and this might be a little confusing, but this is to signify that  the description of the value and the property name pertains to the same thing and that is the value of the variable itself
-this also helps with code readability
-this is commonly used when we try to send information to the back end application
-since we do no thave a front end application yet, we will use the prompt method in order to gather user data
SYNTAX:
JSON.stringify({
    propertyA:variableA,
    propertyB:variableB,
})

*/
let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
    city: prompt("Which city do you live in?"),
    country: prompt ("Which country does you city belong to?")
}

let otherData = JSON.stringify({
    fname:fname,
    lname:lname,
    age:age,
    address:address
})

console.log(otherData);


// SECTION - Parse method
/* 
-converts the stringified JSON into JavaScript Objects 
-Objects are common data types used in applications because fo the complex data structures that can be created out of them
-information is commonly sent to applications in stringified JSON and then converted back into objects 
-this happens for both sending informatoin to a backend application and sending information back to a frontend application
*/

let batchesJSON = `['batchName': 'Batch X', {'batchName': 'Batch Y'}]`;
console.log(batchesJSON);
console.log("Result from parse method");


// console.log(JSON.parse(batchesJSON));
console.log(JSON.parse(data));
console.log(JSON.parse(otherData));